import { History } from "history";
import React from "react";
import { Link } from "react-router-dom";
import {
  Button,
  Card,
  CardBody,
  CardText,
  CardTitle,
  Col,
  Container,
  Row,
} from "reactstrap";
import { Layout } from "../../Components/Layout/Layout";
import { useMeQuery } from "../../generated/graphql";

interface JoinProps {
  history: History;
}

export const Join: React.FC<JoinProps> = ({ history }) => {
  const { data } = useMeQuery();
  return (
    <>
      <Layout history={history}>
        <h1 className="text-center my-4">
          {data?.me?.username}, where would you like to join?
        </h1>
        <Container>
          <Row>
            <Col sm={12} md={6} lg={4}>
              <Card>
                <CardBody>
                  <CardTitle tag="h5">General chat</CardTitle>
                  <CardText>Chat for all users</CardText>
                  <Link to={`/chat?&room=general`}>
                    <Button color="primary">Join</Button>
                  </Link>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </Layout>
    </>
  );
};
