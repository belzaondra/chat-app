import React from "react";
import { Col, Container, Row } from "reactstrap";
import { Layout } from "../../Components/Layout/Layout";
import { GiChatBubble } from "react-icons/gi";
import { History } from "history";

interface HomeProps {
  history: History;
}

export const Home: React.FC<HomeProps> = ({ history }) => {
  return (
    <>
      <Layout history={history}>
        <Container>
          <Row>
            <Col md={12}>
              <h1 className="text-center mt-5">
                Roommates <GiChatBubble />
              </h1>
              <h2 className="text-center my-4">Chat with your friends</h2>
            </Col>
            <Col md={6} className="my-4 d-flex align-items-center">
              <p className="text-justify">
                Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                Aspernatur incidunt, unde animi quibusdam eligendi, eos aliquam
                quod quo quidem dolor quam ex quasi nulla, architecto culpa
                doloribus dicta in. Magni.
              </p>
            </Col>
            <Col md={6} className="my-4">
              <img
                src="chat1.svg"
                alt="chat illustration"
                className="w-100 h-auto"
              />
            </Col>

            <Col md={6} className="my-4">
              <img
                src="chat2.svg"
                alt="chat illustration"
                className="w-100 h-auto"
              />
            </Col>

            <Col md={6} className="my-4 d-flex align-items-center">
              <p className="text-justify">
                Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                Aspernatur incidunt, unde animi quibusdam eligendi, eos aliquam
                quod quo quidem dolor quam ex quasi nulla, architecto culpa
                doloribus dicta in. Magni.
              </p>
            </Col>

            <Col md={6} className="my-4 d-flex align-items-center">
              <p className="text-justify">
                Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                Aspernatur incidunt, unde animi quibusdam eligendi, eos aliquam
                quod quo quidem dolor quam ex quasi nulla, architecto culpa
                doloribus dicta in. Magni.
              </p>
            </Col>
            <Col md={6} className="my-4">
              <img
                src="chat3.svg"
                alt="chat illustration"
                className="w-100 h-auto"
              />
            </Col>
          </Row>
        </Container>
      </Layout>
    </>
  );
};
