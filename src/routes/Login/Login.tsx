import { Form, Formik } from "formik";
import React from "react";
import { Button, Col, Container, Row } from "reactstrap";
import * as Yup from "yup";
import { InputField } from "../../Components/Inputs/InputField";
import { Layout } from "../../Components/Layout/Layout";
import { MeDocument, MeQuery, useLoginMutation } from "../../generated/graphql";
import { History } from "history";

const LoginSchema = Yup.object().shape({
  password: Yup.string()
    .min(2, "Too Short!")
    .max(50, "Too Long!")
    .required("Required"),
  email: Yup.string()
    .min(2, "Too Short!")
    .max(50, "Too Long!")
    .email("Invalid email!")
    .required("Required"),
});

interface LoginProps {
  history: History;
}

export const Login: React.FC<LoginProps> = ({ history }) => {
  const [login] = useLoginMutation();
  return (
    <>
      <Layout history={history}>
        <Container>
          <h1 className="text-center my-4">Login</h1>
          <Row className="flex justify-content-center align-items-center">
            <Col md="6" className="mt-4">
              <Formik
                initialValues={{
                  email: "admin@admin.com",
                  password: "Admin",
                }}
                validationSchema={LoginSchema}
                onSubmit={async (values) => {
                  console.log();

                  await login({
                    variables: values,
                    update: (cache, { data }) => {
                      cache.writeQuery<MeQuery>({
                        query: MeDocument,
                        data: {
                          __typename: "Query",
                          me: data?.login?.user,
                        },
                      });
                    },
                  });

                  history.push("join");
                }}
              >
                {({ errors, touched, values }) => (
                  <Form>
                    <InputField
                      name="email"
                      label="Email"
                      placeholder="example@example.com"
                    />
                    <InputField
                      name="password"
                      label="Password"
                      type="password"
                    />

                    <Button type="submit" color="primary" block>
                      Login
                    </Button>
                  </Form>
                )}
              </Formik>
            </Col>
          </Row>
        </Container>
      </Layout>
    </>
  );
};

export default Login;
