import queryString from "query-string";
import React, { useEffect, useState } from "react";
import {
  Alert,
  Button,
  Col,
  Container,
  Form,
  FormGroup,
  Input,
  Row,
} from "reactstrap";
import { io, Socket } from "socket.io-client";
import { DefaultEventsMap } from "socket.io-client/build/typed-events";
import { Layout } from "../../Components/Layout/Layout";
import { History } from "history";
import { useMeQuery } from "../../generated/graphql";

interface ChatProps {
  location: Location;
  history: History;
}
interface Message {
  user: string;
  message: string;
  id: string;
  userId: number;
}
let socket: Socket<DefaultEventsMap, DefaultEventsMap>;

export const Chat: React.FC<ChatProps> = ({ location, history }) => {
  const { data, loading } = useMeQuery();
  const [room, setRoom] = useState("");
  const [message, setMessage] = useState("");
  const [messages, setMessages] = useState<Message[]>([]);
  const server = "http://localhost:4000";

  useEffect(() => {
    if (!loading && data?.me?.id) {
      const { room } = queryString.parse(location.search);

      setRoom(room as string);

      socket = io(server, { transports: ["websocket"] });

      socket.emit("joinRoom", { userId: data?.me?.id, room }, () => {});
    }

    return () => {
      if (!socket) return;
      socket.disconnect();
      socket.off();
    };
  }, [server, location.search, data?.me?.id, loading]);

  useEffect(() => {
    if (!socket) return;
    socket.on("message", (message: Message) => {
      console.log("message", message);
      setMessages([...messages, message]);
    });

    socket.on("loadMessages", (messages: Message[]) => {
      setMessages(messages);
    });
  }, [messages, socket]);

  const sendMessage = (e: any) => {
    e.preventDefault();
    socket.emit("sendMessage", message, () => {});
    setMessage("");
  };

  return (
    <>
      <Layout history={history}>
        <Container className="h-100">
          <Row>
            <Col md={12}>
              <h2 className="text-center">Chat {room}</h2>
              {messages.map((m) => (
                <Row key={m.id}>
                  <Col sm={12}>
                    <div
                      className={`w-75 ${
                        m.userId === data?.me?.id ? "float-right" : ""
                      }`}
                    >
                      <span className="text-muted">
                        {m.userId === data?.me?.id ? "You" : m.user}
                      </span>
                      <Alert color="primary">{m.message}</Alert>
                    </div>
                  </Col>
                </Row>
              ))}
            </Col>
          </Row>
          <Row className="align-items-end mt-auto">
            <Col md={12}>
              <Form inline>
                <FormGroup className="w-75">
                  <Input
                    type="text"
                    name="message"
                    id="message"
                    className="w-100"
                    value={message}
                    placeholder="Message..."
                    onChange={(e) => setMessage(e.target.value)}
                    onKeyPress={(e) =>
                      e.key === "Enter" ? sendMessage(e) : null
                    }
                  />
                </FormGroup>
                <Button
                  type="button"
                  className="w-25"
                  color="primary"
                  onClick={(e) => sendMessage(e)}
                >
                  Send
                </Button>
              </Form>
            </Col>
          </Row>
        </Container>
      </Layout>
    </>
  );
};
