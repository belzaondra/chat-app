import { Form as FormikForm, Formik } from "formik";
import React from "react";
import { Button, Col, Container, Row } from "reactstrap";
import * as Yup from "yup";
import { InputField } from "../../Components/Inputs/InputField";
import { Layout } from "../../Components/Layout/Layout";
import {
  MeDocument,
  MeQuery,
  useRegisterMutation,
} from "../../generated/graphql";
import { History } from "history";

const SignupSchema = Yup.object().shape({
  username: Yup.string()
    .min(2, "Too Short!")
    .max(50, "Too Long!")
    .required("Required"),
  password: Yup.string()
    .min(2, "Too Short!")
    .max(50, "Too Long!")
    .required("Required"),
  email: Yup.string()
    .min(2, "Too Short!")
    .max(50, "Too Long!")
    .email("Invalid email!")
    .required("Required"),
});

interface RegisterProps {
  history: History;
}

export const Register: React.FC<RegisterProps> = ({ history }) => {
  const [register] = useRegisterMutation();
  return (
    <>
      <Layout history={history}>
        <h1 className="text-center my-4">Register</h1>
        <Container>
          <Row className="flex justify-content-center align-items-center">
            <Col md="6" className="mt-4">
              <Formik
                initialValues={{
                  username: "",
                  email: "",
                  password: "",
                }}
                validationSchema={SignupSchema}
                onSubmit={async (values) => {
                  await register({
                    variables: { options: values },
                    update: (cache, { data }) => {
                      cache.writeQuery<MeQuery>({
                        query: MeDocument,
                        data: {
                          __typename: "Query",
                          me: data?.register.user,
                        },
                      });
                    },
                  });

                  history.push("join");
                }}
              >
                {({ errors, touched, values }) => (
                  <FormikForm>
                    <InputField
                      name="username"
                      label="Username"
                      placeholder="Username..."
                    />
                    <InputField
                      name="email"
                      label="Email"
                      placeholder="example@example.com"
                    />
                    <InputField
                      name="password"
                      type="password"
                      label="Password"
                      placeholder="example@example.com"
                    />

                    <Button type="submit" color="primary" block>
                      Register
                    </Button>
                  </FormikForm>
                )}
              </Formik>
            </Col>
          </Row>
        </Container>
      </Layout>
    </>
  );
};

export default Register;
