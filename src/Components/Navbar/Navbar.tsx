import { useApolloClient } from "@apollo/client";
import React from "react";
import { GiChatBubble, GiPartyPopper } from "react-icons/gi";
import { Link } from "react-router-dom";
import {
  Container,
  Nav,
  Navbar as BNavbar,
  NavItem,
  NavLink,
} from "reactstrap";
import { useLogoutMutation, useMeQuery } from "../../generated/graphql";
import { History } from "history";

interface NavbarProps {
  history: History;
}

export const Navbar: React.FC<NavbarProps> = ({ history }) => {
  const { data } = useMeQuery();
  const [logout] = useLogoutMutation();
  const apolloClient = useApolloClient();

  return (
    <>
      <BNavbar color="light" light expand="md">
        <Container>
          <Link to={`/`} className="navbar-brand">
            Roommates <GiChatBubble />
          </Link>
          <Nav className="ml-auto" navbar>
            {!data?.me ? (
              <>
                <NavItem>
                  <Link to={`/login`} className="nav-link">
                    Login
                  </Link>
                </NavItem>

                <NavItem>
                  <Link to={`/register`} className="nav-link">
                    Sign up
                  </Link>
                </NavItem>
              </>
            ) : (
              <>
                <NavItem>
                  <Link to={`/join`} className="nav-link">
                    Start chatting <GiPartyPopper />
                  </Link>
                </NavItem>

                <NavItem>
                  <NavLink
                    href="#"
                    className="nav-link"
                    onClick={async () => {
                      await logout();
                      await apolloClient.resetStore();
                      history.push("/");
                    }}
                  >
                    Logout
                  </NavLink>
                </NavItem>
              </>
            )}
          </Nav>
        </Container>
      </BNavbar>
    </>
  );
};
