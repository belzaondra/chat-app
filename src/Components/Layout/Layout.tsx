import React, { ReactNode } from "react";
import { Navbar } from "../Navbar/Navbar";
import { History } from "history";

interface LayoutProps {
  children?: ReactNode;
  history: History;
}

export const Layout: React.FC<LayoutProps> = ({ children, history }) => {
  return (
    <>
      <Navbar history={history} />
      {children}
    </>
  );
};
