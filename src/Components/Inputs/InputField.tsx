import { useField } from "formik";
import React from "react";
import { FormGroup, Label, Input, FormFeedback } from "reactstrap";
import { InputType } from "reactstrap/es/Input";

type InputFieldProps = React.InputHTMLAttributes<HTMLInputElement> & {
  name: string;
  label: string;
  type?: InputType;
  textarea?: boolean;
};

export const InputField: React.FC<InputFieldProps> = ({
  label,
  textarea,
  type,
  size: _,
  ...props
}) => {
  const [field, { error, touched }] = useField(props);
  return (
    <>
      <FormGroup>
        <Label for={field.name}>{label}: </Label>
        <Input
          {...field}
          type={type || "text"}
          id={field.name}
          placeholder={props.placeholder}
          invalid={!!error && touched}
        />
        {error ? <FormFeedback>{error}</FormFeedback> : null}
      </FormGroup>
    </>
  );
};
