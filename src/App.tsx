import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import { Chat } from "./routes/Chat/Chat";
import { Home } from "./routes/Home/Home";
import { Join } from "./routes/Join/Join";
import Login from "./routes/Login/Login";
import { Register } from "./routes/Register/Register";

const App = () => {
  return (
    <Router>
      <Route path="/" exact component={Home} />
      <Route path="/join" exact component={Join} />
      <Route path="/chat" component={Chat} />
      <Route path="/login" exact component={Login} />
      <Route path="/register" exact component={Register} />
    </Router>
  );
};

export default App;
